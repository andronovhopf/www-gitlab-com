---
layout: handbook-page-toc
title: Learning & Development Newsletter
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This page covers the process our Learning and Development (L&D) team follows for creating the quarterly L&D Newsletter! 

To see currently availble editions of our L&D Newsletter, see our [Learning Culture]() page. 

## Newsletter Process 

The newsletter is live **on the first Wednesday of each Quarter.**  

We build the newsletter in an issue in the [Learning & Development Newsletter project](https://gitlab.com/gitlab-com/people-group/learning-development/newsletter). The process for the issue includes: 
- The issue for the upcoming newsletter is opened at least three weeks before the publish date. 
- Relevant stakeholders/contributors are tagged in the issue each month to provide content or review suggested content in the outline. 
- Once the outline is complete, L&D team drafts the newsletter in a WIP MR. 
- Reviewers provide feedback no later than 3 pm CT two business days before the planned go-live date to allow time for revisions.
- L&D Team announces the newsletter via slack on go live date. 
- Once the newsletter goes live, the L&D Team sends a slack notification in the #whats-happening-at-gitlab, #learninganddevelopment, and #managers Slack channels 
- Once the newsletter is live, the L&D team closes out the planning issue.  

### Template

The following template is the typical template for each newsletter. When creating the newsletter, the template can be copied, pasted, and then filled in accordingly. 

```
---
layout: handbook-page-toc
title: {add date - ex. FY21-Q3} L&D Newsletter
---

{add brief intro}

## Recap of Q[3]

* {add a bulleted list}

Past Live Learning sessions can be found on our [Learning & Development Page](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning). 

## Upcoming in [Q4] 

* {add a bulleted list}

Sign up for our upcoming Live Learning events in [Q4] by using [Signup Genius?]()

## Learning Spotlight 

* {add a bulleted list}

## Video 

[embed video]

## Department Spotlight 

* {add a bulleted list}

## Learn from Leadership 

* {add a bulleted list}

## Learner Spotlight  

* {add a bulleted list}

## Learning Tips 

* Set aside 2 hours per week to work on personal development
* {add a bulleted list}

## Other Enablement Initiatives

* {add a bulleted list}

## Discussion 

If you would like to discuss items related to this newsletter, please see the related [issue](). 

```
