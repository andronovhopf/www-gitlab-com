---
layout: handbook-page-toc
title: "Solution Architect Mentorship Program (SAMP)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goals

The Solution Architect Mentorship Program (SAMP) supports GitLab Values of Collaboration and Diversity, Inclusion, and Belonging by purposefully creating and cultivating opportunities for mentors and mentees to engage over critical questions in the organization and in their professional lives.

The goals of the Solution Architect Mentorship Program are to provide informal professional and career guidance to Solution Architects and to assist team members in meeting their career goals by honing new capabilities.

If successful, we will consider an expansion to other groups across the company. In the spirit of iteration, we would like to get started soon and expand and improve as we go.

## Program structure

The program proposes a mentor/mentee relationship between SA mentees and mentors. Sessions will take place every other week for 30 minutes, or as agreed upon between the mentor and mentee, as long as both mentors and mentees remain engaged. In addition to mentorship during the sessions, the mentor might suggest opportunities for networking, public speaking, leadership development, or shadowing as appropriate for the mentee's needs.

Any member of the Solution Architecture organization may volunteer to be a mentor and/or a mentee. The Solution Architecture management team will assign mentors to mentees as they sign up, on a continuous basis. Solution Architecture management might also, from time to time, assign members of their team to become a mentor or a mentee.

Note that mentorship might include discussion related to career progression, but the program is for any Solution Architect who wants to improve in their role, regardless of whether they are seeking a promotion.

## FAQs

**How do I sign up?**

Two options: Join the `#samp` channel and state your intention there, or discuss with your manager.

**Can I be both a mentor and a mentee?**

Yes!

**Can I mentor and/or mentee more than one person?**

Probably not; we all have work to do!

**If the mentor/mentee relationship doesn't work out, can I switch?**

Yes; discuss it with your manager.

**I'm not good at this. How do I learn more?**

A great book on coaching (which is similar to mentoring) is [The Coaching Habit: Say Less, Ask More & Change the Way You Lead Forever](https://www.amazon.com/Coaching-Habit-Less-Change-Forever/dp/0978440749). It outlines some basic guidelines for how to be an effective mentor.

