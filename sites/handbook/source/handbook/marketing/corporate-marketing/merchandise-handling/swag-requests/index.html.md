---
layout: handbook-page-toc
title: "Merchandise Handling"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Requesting Swag

This page outlines ways that GitLab team members can request swag. If you are looking to host a giveaway of GitLab swag, please review the [GitLab Giveaways process](/handbook/marketing/corporate-marketing/merchandise-handling/giveaways/).

### Important Notes

We recommend that you request merchandise **at least 4 weeks in advance** for us to be able to accommodate your request. If your request is urgent, please reach out to the community advocates via <merch@gitlab.com> to find out if an expediated shipping option is available.

Other ways you can get support for your swag request:
* Tag the @advocates in the [#swag Slack channel](https://app.slack.com/client/T02592416/C66R8N98F/thread/CB16DMSLC-1594317317.394500)
* In order to keep the orders transparent, please do not send requests via direct messages. Instead, email <merch@gitlab.com>.


## Types of Swag Requests

GitLab team members can use the Unfiltered Youtube Channel to watch the following training: [Swag Requests](https://youtu.be/l36QBqHn0rE)

Please use this table to confirm you're using the correct order process for GitLab Swag:

| Swag Order Scenario | Order Process|
| --- | --- |
| You're speaking at an upcoming event and would like to bring GitLab swag | Follow the [Swag for Speaking at or Hosting GitLab events process](/handbook/marketing/corporate-marketing/merchandise-handling/swag-requests/#swag-for-speaking-at-or-hosting-gitlab-events) |
| You're hosting an event and you'd like to share GitLab swag | Follow the [Swag for Speaking at or Hosting GitLab events process](/handbook/marketing/corporate-marketing/merchandise-handling/swag-requests/#swag-for-speaking-at-or-hosting-gitlab-events) |
| You're ordering swag for a customer | Use the [Printfection Sales Swag link](https://get.printfection.com/dcdzm/6508378270). This link is for GitLab internal use only and requires login with a @gitlab.com email address. If you have trouble signing into Printfection, ping the #swag slack channel for support. Review the [Swag for customer/ prospects](/handbook/marketing/corporate-marketing/#swag-for-customer-prospects) for guidelines on order sizes for customers. |
| You want to recognize a wider community member for their contributions | Submit the form for [GitLab Nominations for Community Swag](/handbook/marketing/corporate-marketing/merchandise-handling/community-rewards-internal/) |
| You want to organize a giveaway | Follow the [Giveaway process](/handbook/marketing/corporate-marketing/merchandise-handling/giveaways/) |
| You want to recognize an MVP on release day | Follow the [MVP appreciation gifts process]((/handbook/marketing/corporate-marketing/merchandise-handling/swag-requests/#mvp-appreciation-gifts)


### Swag for Speaking at or Hosting GitLab Events

GitLab team members can request a merch order for an event they are speaking at or hosting by emailing the request to <merch@gitlab.com>.

Please use the following email template when placing a merchandise request:

```markdown
Hello Merch Team,

I'd like to place the following GitLab merchandise order:

Reason for Order: 

Item Name: 
Size, if applicable: 
Quantity:


Name for the order:
Shipping Address:
Email Address:
Phone Number:


Regards,
YOUR_NAME
```


