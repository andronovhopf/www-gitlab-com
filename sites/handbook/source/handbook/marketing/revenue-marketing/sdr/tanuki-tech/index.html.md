---
layout: markdown_page
title: "Tanuki Tech"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

## Tanuki Tech

Tanuki Tech (short for "Tanuki Instutite of Technology") is the world class sales/technology bootcamp that we are building out for the SDR organization. Our goal is to powerfully equip our SDRs to be successful in their current roles as well as prepare them for their next role, whatever that may be. We are doing this because we care about our SDRs and want them to be successful.

## Goals
How do we measure success? The specific goals for this program are:
* Increased SDR effectiveness and organizational efficiency (increased quota attainment).
* Decrease time to value for SDR new hires (we want to equip our new SDRs to be successful quickly).
* **Job placement for SDRs who complete our program.** We want the people that we develop here as part of our SDR program to be abundantly prepared for their next role; whether that's in sales, marketing, or wherever else.

The average coding bootcamp here in the United States costs 13.5k and graduates report an average starting salary of 67k (CourseReport 2020). Our goal is to far exceed the job placement and starting salary metrics of our peer institutions, **completely as a free value to our SDRs as part of being part of this organization.**

## Guiding Principles
* Enablement should be powerful and effective. If there is something that you feel would help our organization and is currently missing or have suggestions for improvement, let us know.
  * At the end of each class, we will give surveys to figure out what's working and how we can improve.
* Enablement should be quick and effective. We are not trying to inundate you with information to eat up all of your time. Most courses in our curriculum are designed to take up around three hours including examination.
* We test often and we test behaviors that apply to your actual job. Research shows that the most effective teachers consistently test more than their peers. **Do not get discouraged if you fail a test; it is better to fail in front of a peer who is trying to help you than in front of a customer.**
* Class sizes should be small (especially for the 100- and 200-level courses. We aim to have a teacher-to-student ratio of 7:1 for 100- and 200-level courses.
* All classes are offered through Zoom and additionally have an optional asynchronous option (recordings and documentation). Examinations are either written or through Zoom.

## Curriculum

**100s -**

TT100: Introduction to Technology
> Serves as an introduction to the technology industry and GitLab's space in it. In this course, we will explain fundamental concepts such as applications, operations, and the hybrid cloud. We will also explain why technology has been the main driver of innovation in the twenty-first century.
> 
> 3 credit hours

TT101: Introduction to GitLab
> GitLab is an extremely promising suite of technologies that is broad as well as deep. In this course, we will introduce GitLab and explain why it is such a special piece of technology.
> 
> 3 credit hours

TT102: The People of Technology
> To effectively engage in customer conversations, we need to understand the people that make up the technology industry. In this course, we will explain the various people that you will meet, their responsibilities and drivers, and how to have effectively engage each type of audience.
>
> 3 credit hours

TT110: Competitive
> Because we operate in so many different product categories, GitLab has a large number of competitors. In this class, we discuss our biggest four competitors and how to effectively handle competitive situations with them.
>
> 3 credit hours

**200s -**

TT200: Introduction to Development
> In this class, our goal is to have more effective conversations with developers by understanding what they do and learning to speak their language. Topics include life as a developer, how software is developed, where applications run, the public cloud, and APIs.
>
> 3 credit hours

TT210: GitLab Create and Plan
> In this class, we go indepth explaining create and plan functionality. Topics include GitLab's SCM and Agile featureset and competitive differentiators.
>
> 3 credit hours

TT211: GitLab Verify and Security
> In this class, we will learn about verify and security functionality. Topics include waterfall versus agile, continuous integration, software testing, devsecops, and automation.
>
> 3 credit hours

TT220: GitLab Package, Configure, and Release
> In this class, we will learn about package, configure, and release functionality. Topics include how software is packaged and ultimately released (rolling deployments, A/B testing, etc.).
>
> 3 credit hours

TT330: GitLab Manage, Monitor, and Protect
> In this class, we will learn about manage, monitor, and protect functionality. Topics include how we allow engineering leaders to make proactive decisions to optimize their organizations and infrastructure.
>
> 3 credit hours

**300s -**

TT301: AWS and Google Alliances
> In this class, we will learn about what alliances are and discuss our co-sell motions with Amazon and Google. Great for those that are planning on working AWS/Google events.
>
> 3 credit hours

TT310: Demoing GitLab
> In this class, we will practice demoing GitLab. After taking this class, you will be able to perform brief customer demos to gather customer interest and explain product value.
>
> 5 credit hours

TT330: Career Builder
> In this class, we will discuss best practices for plotting your career direction and how to get there. Included in this class is "Sales Stories," where we interview sales leaders to help you determine which path to take your career.
>
> 5 credit hours

TT380: Technical Questions for Sales
> This is a class offered by the sales development organization equipping salespeople to answer the most commonly asked technical questions. You may see the curriculum [here](/handbook/sales/training/technical-questions-for-sales/). This class is delivered async only.
>
> 5 credit hours

## Examination
We test for mastery (mastery-based learning) and students will be asked to demonstrate knowledge/skills learned. Students will be asked to repeat examination (or update answers) until mastery is demonstrated and the number of attempts needed is [tracked](https://docs.google.com/spreadsheets/d/1P7QvZ09F8OT7LJTeMRYbu6_Oxjw9UdouaJ5Oeo8SU8k/edit?usp=sharing).

## Enrollment

To enroll for a class:
* Surveys will go out the first two weeks of each quarter.
* **Live classes are filled on a first-come-first-serve basis and there is no guarantee that you will be given a spot**. Once again, all classes have an asynchronous option so if a class is full, you can always watch a recording and have the opportunity to test for credit.
* Class enrollment is done by SignUpGenius.

## SDR Leveling

Completing coursework is directly tied to SDR leveling.
* To qualify for SDR2, you must complete all of the 100-level courses.
* To qualify for senior SDR/SDR3, you must complete all of the 200-level courses.

## Dean of Tanuki Tech

[Christopher Wang](https://about.gitlab.com/company/team/#cs.wang) currently serves as the first dean of Tanuki Tech.
