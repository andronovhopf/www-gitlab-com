module FeatureHelpers
  def missing_feature(feature)
    !(feature.gitlab_core || feature.gitlab_starter || feature.gitlab_premium || feature.gitlab_ultimate)
  end

  def get_carousel_item(title, link, image, type)
    item_object = { title: title, link: link, image: image, type: type }
    Struct.new(*item_object.keys).new(*item_object.values)
  end
end
