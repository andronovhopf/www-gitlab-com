---
layout: markdown_page
title: Product Direction - Product Analytics
description: "Product Analytics manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
canonical_path: "/direction/product-analytics/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

The vision of Product Analytics is to help us build a better Gitlab. We collect data about how GitLab is used to understand what product features to build, to understand how to serve our customers, and to help our customers understand their DevOps lifecycle.

## Roadmap

### Privacy Policy and Settings - Rollout Updated Privacy Policy

Product Analytics Steps: Privacy Policy, Privacy Settings

Teams Involved: Legal, Product Analytics, Data

**Why This Is Important:**

- Our [privacy policy](https://about.gitlab.com/privacy/) has not been updated since the [October 29, 2019 Update on free software and telemetry](https://about.gitlab.com/blog/2019/10/10/update-free-software-and-telemetry/). This policy needs to be updated to align with our Product Analytics direction.
- Privacy settings in GitLab are currently scattered in several places.

**Recent Progress:**

- WIP

**What's Next:**

- [Rollout an updated Privacy Policy](https://gitlab.com/groups/gitlab-com/-/epics/907)
- Build a centralized privacy settings page in GitLab for users to easily understand and control their privacy settings.

### Data Collection - Maintain and Scale Our Collection Framework

Product Analytics Steps: Collection Framework, Event Dictionary, Instrumentation

Teams Involved: Product Analytics

**Why This Is Important:**

- Product Metrics are currently instrumented by all product teams.
- We're close to 3x the number of metrics tracked since the beginning of this year, as such, we need to ensure metrics are added in a structured way.

**Recent Progress:**

- Over 800 metrics tracked in Usage Ping
- Product Analytics review process
- Usage Ping and Snowplow documentation
- Client and server-side counts can now be tracked on SaaS and Self-Managed.
- Client and server-side events are a work in progress on Self-Managed.
- Plan-level reporting using SaaS Usage Ping is not possible as SaaS is multi-tenant and Usage Ping reports at an Instance level.
- We've implemented an [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174)

**What's Next:**

- Restructure Usage Ping with nesting hierarchy and naming convention (time period, segmentation, event name)


### Data Collection - Snowplow Event Tracking on Self Managed

Product Analytics Steps: Collection Framework, Event Dictionary, Instrumentation

Teams Involved: Product Analytics

**Why This Is Important:**

- WIP

**Recent Progress:**

- WIP

**What's Next:**

- We need to implement an Event Dictionary for Snowplow
- Usage Ping Snowplow on Self-Managed (using internal collector and database for moderate volume)
- Group-level, Project-level, User-level tracking for Snowplow on SaaS


### Processing Pipeline - Decrease Cycle Times for Product Analytics

Product Analytics Steps: Release Cycle, Product Usage, Usage Ping Generation, Collectors, Extractors, Loaders, Snowflake Data Warehouse, dbt Data Model

Teams Involved: Product Analytics, Data, Product Managers

**Why This Is Important:**

- Data Availability cycle times are currently a 51 day cycle
- Exports of the Versions DB are currently done manually every 14 days according to [this schedule](https://gitlab.com/groups/gitlab-data/-/epics/162).

**Recent Progress:**

- WIP

**What's Next:**

- [Automate Versions DB exports](https://gitlab.com/gitlab-org/product-analytics/-/issues/398) to reduce cycle times by 14 days.


### Processing Pipeline - Plan and Group-level Reporting for SaaS

Product Analytics Steps: Collection Framework, Usage Ping Generation, Collectors, Processors, Snowflake Data Warehouse, dbt Data Model

Teams Involved: Product Analytics, Data 

**Why This Is Important:**

- Currently Usage Ping is not segmented by plan type which means for any SaaS free / paid segmentation cannot be done using Usage Ping. Instead, as a work around, the data team is using the Postgres database import and manually recreating all Usage Ping queries in Sisense.

**Recent Progress:**

- WIP

**What's Next:**

- [Plan-level and Group-level reporting for Usage Ping Postgres on SaaS for multi-tenant reporting](https://gitlab.com/gitlab-org/product-analytics/-/issues/423)
- Shift the SaaS usage ping workload into the data warehouse. A single usage ping on SaaS takes 24 hours to generate, for group level metrics, we need to run this 1 million times which is only feasible if it's done in the data warehouse.

### Enable Product Teams with Product Analytics - Lead FY21-Q4 Product OKR1 - KR1 100% of DevOps groups have Projected GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets

Product Analytics Steps: Instrumentation, Sisense Dashboard, Performance Indicators, Metrics Reviews, Product Improvements

Teams Involved: Product Managers, Product Analytics, Data

**Why This Is Important:**

- Currently, for our SMAU / Section MAU metrics, we use the highest component in each grouping. For example, for SMAU, we take the highest GMAU in the group. This methodology of calculating SMAU / Section MAU is lacking as uses one metric to represent the whole group instead of finding the union across the entire group.
- Some stages and groups are still instrumenting their primary performance indicators.
- Sisense dashboards are custom made and difficult to compare across stages and groups.
- Targets are defined by PMs based off little data.

**Recent Progress:**

- All sections, stages, and groups have primary performance indicators defined and being instrumented ([Dev](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/), [Ops](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/), [Secure and Protect](https://about.gitlab.com/handbook/product/secure-and-protect-section-performance-indicators/), [Enablement](https://about.gitlab.com/handbook/product/enablement-section-performance-indicators/))
  - KR1 (EVP, Product): [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
  - KR2 (EVP, Product): [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343) 
- All [stage and group performance indicators](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) have been added to the handbook. 
- PI Reviews now use Section PI pages instead of slide decks.

**What's Next:**

- Create a standardized dashboard template that can be reused across all PIs.
- Create process to set PI targets. Allow targets to be dynamically set.
- Create Monthly PI review process.
- Create [de-duplicated GMAU and SMAU metrics](https://gitlab.com/gitlab-org/product-analytics/-/issues/421) in Usage Ping before the Usage Ping is sent. 
- Plan-level reporting for Usage Ping Redis on SaaS for multi-tenant reporting


### Enable Sales and Customer Success Teams with Product Analytics

Product Analytics Steps: Salesforce Data Feed, Salesforce Dashboard, Gainsight Dashboard, Customer Conversations

Teams Involved: Sales, Customer Success, Product Analytics, Data

**Why This Is Important:**

- License data has been added to this data feed

**Recent Progress:**

- Support the [FY21-Q3 Deploying Product Analytics for CRO Org](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#). For more information, see [CRO Product Analytics: Status, Gaps and the Road Forward](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#).
- We've [Replicate Versions App Reporting in Sisense Dashboards](https://gitlab.com/gitlab-data/analytics/-/issues/4488/)

**What's Next:**

- Add Usage Ping data into Sales Data Feed.
- [Customer Adoption Journey](https://gitlab.com/groups/gitlab-org/-/epics/3572)


### Enable Full Funnel Analytics 

Product Analytics Steps: Marketing Data, Product Data, Fulfillment Data, Sales Data, Customer Success Data, Full Funnel Analysis

Teams Involved: Marketing, Fulfillment, Sales, Customer Success, Product Analytics, Data, Enterprise Applications

**Why This Is Important:**

- WIP

**Recent Progress:**

- WIP

**What's Next:**

- WIP

### Make Product Analytics Data Useful For Customers

Product Analytics Steps: Value Stream Data, Value Stream Features

Teams Involved: Manage, Product Analytics

**Why This Is Important:**

- WIP

**Recent Progress:**

- WIP

**What's Next:**

- WIP

### Customer Product Analytics Feature

Product Analytics Steps: Collection Framework

Teams Involved: Product Analytics

**Why This Is Important:**

- WIP

**Recent Progress:**

- WIP

**What's Next:**

- WIP

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Product Analytics Guide](/handbook/product/product-analytics-guide)                                                                              | A guide to Product Analytics                   |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html)                                              | An implementation guide for Usage Ping                    |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/product_analytics/snowplow.html)                                                  | An implementation guide for Snowplow                      |
| [Event Dictionary](/handbook/product/product-analytics-guide#event-dictionary)                                        | A SSoT for all collected metrics and events               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Implementing Product Performance Indicators](/handbook/product/product-analytics-guide#implementing-product-performance-indicators)                                   | The workflow for putting product performance indicators in place   |
| [Product Analytics Direction](/direction/product-analytics/)                                                                              | The roadmap for Product Analytics at GitLab                       |
| [Product Analytics Development Process](/handbook/engineering/development/growth/conversion-product-analytics/) | The development process for the Product Analytics groups         |
